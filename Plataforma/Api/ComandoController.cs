﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using COMMON.Entidades;
using COMMON.Interfaces;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Plataforma.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComandoController : GenericController<Comando>
    {
        public ComandoController() : base(new GenericRepository<Comando>())
        {
        }
    }
}