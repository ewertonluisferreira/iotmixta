﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using COMMON.Entidades;
using COMMON.Interfaces;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Plataforma.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class DispositivoController : GenericController<Dispositivo>
    {
        public DispositivoController() : base(new GenericRepository<Dispositivo>())
        {
        }
    }
}