﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Entidades
{
    public class Lectura:BaseDTO
    {
        public ObjectId IdDispositivo { get; set; }
        public float Temperatura { get; set; }
        public float Humedad { get; set; }
        public int Pulsador { get; set; }
        public float Luminosidad { get; set; }
    }
}