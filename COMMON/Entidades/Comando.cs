﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Entidades
{
    public class Comando:BaseDTO
    {
        public ObjectId IdDispositivoDestino { get; set; }
        public ObjectId IdDispositivoOrigen { get; set; }
        public string ComandoTexto { get; set; }
    }
}
