﻿using COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COMMON.Interfaces
{
    public interface IGenericRepository<T> where T:BaseDTO
    {
        T Create(T entidad);
        IQueryable<T> Read { get; }
        T Update(T entidad);
        bool Delete(ObjectId id);

        T SearchById(ObjectId id);
        IQueryable<T> Query(Expression<Func<T, bool>> predicado);
    }
}
