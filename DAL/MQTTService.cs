﻿using OpenNETCF.MQTT;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace DAL
{
    public class MQTTService
    {
        private MQTTClient client;
        public event EventHandler<string> MensajeRecibido;
        public bool Conectado { get; private set; }
        public MQTTService(string servidor,int puerto, string user, string password,string cliente)
        {
            client = new MQTTClient(servidor, puerto);
            int intentos = 0;
            Debug.WriteLine("Intentando conectar con servidor MQTT:");
            client.Connect(cliente, user, password);
            while (!client.IsConnected)
            {
                Thread.Sleep(1000);
                Debug.Write(".");
                intentos++;
                if (intentos == 120)
                {
                    Debug.WriteLine("\nNo se pudo conectar...");
                    Conectado = false;
                    return;
                }
            }
            Debug.WriteLine("Conectado Correctamente :-)");
            Conectado = true;
            client.MessageReceived += Client_MessageReceived;
        }

        public MQTTService(string servidor, int puerto, string cliente)
        {
            client = new MQTTClient(servidor, puerto);
            int intentos = 0;
            Debug.WriteLine("Intentando conectar con servidor MQTT:");
            client.Connect(cliente);
            while (!client.IsConnected)
            {
                Thread.Sleep(1000);
                Debug.Write(".");
                intentos++;
                if (intentos == 120)
                {
                    Debug.WriteLine("\nNo se pudo conectar...");
                    Conectado = false;
                    return;
                }
            }
            Debug.WriteLine("Conectado Correctamente :-)");
            Conectado = true;
            client.MessageReceived += Client_MessageReceived;
        }

        private void Client_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            string mensaje = System.Text.Encoding.UTF8.GetString(payload);
            Debug.WriteLine($"[{topic}]<-{mensaje}");
            MensajeRecibido(this, mensaje);
        }

        public bool Suscribir(string tema)
        {
            if (client.IsConnected)
            {
                client.Subscriptions.Add(new Subscription(tema));
                Debug.WriteLine("Suscripción Correcta");
                return true;
            }
            else
            {
                Debug.WriteLine("NO Conectado");
                return false;
            }

        }

        public bool Publicar(string tema, string mensaje)
        {
            if (client.IsConnected)
            {
                Debug.WriteLine($"[{tema}]->{mensaje}");
                try
                {
                    client.Publish(tema, mensaje, QoS.FireAndForget, false);
                    Debug.WriteLine("Enviado");
                    return true;
                }
                catch (Exception)
                {
                    Debug.WriteLine("NO Enviado");
                    return false;
                }
            }
            else
            {
                Debug.WriteLine("NO Conectado");
                return false;
            }
        }

    }
}
